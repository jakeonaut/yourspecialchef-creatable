function GoHome(){
	window.location.assign("http://yourspecialchef.weebly.com/");
}

function DeselectButtons(){
	var elements = $(".selected");
	for (var i = 0; i < elements.length; i++){
		elements[i].className = elements[i].className.replace(/\s*selected\s*/g, "");
	}
	
	elements = $(".filler");
	for (var i = 0; i < elements.length; i++){
		var hd = document.createElement('div');
		hd.className = 'hd';
		elements[i].innerHTML = "";
		elements[i].appendChild(hd);
		elements[i].className = "horizontal_divider";
	}
}

function Select(e){
	var button = e.toElement;
	DeselectButtons();
	button.className += " selected";
	
	//SET THE FILLER
	var row = button.parentNode.nextSibling;
	while (row && row.nodeType != 1){
		row = row.nextSibling;
	}
	row.className = "filler";
	
	//POPULATE THE FILLER
	if (button.id === "recipe") SelectRecipe(row);
	if (button.id === "new_step") SelectNewStep(row);
	if (button.id === "settings") SelectSettings(row);
}

function SelectRecipe(filler){
	filler.innerHTML = "&nbsp;";
}

function SelectNewStep(filler){
	filler.innerHTML = "";
	//Append new section
	var section = CreateNewSection("blue", "section", true);
	section.style.margin = "12px";
	filler.appendChild(section);
	
	//Append new item
	var item = CreateNewItem("blue", "item", "content/images/shreddedcheese.jpg", true);
	item.style.margin = "12px";
	item.style.marginTop = "0px";
	filler.appendChild(item);
}

function SelectSettings(filler){
	filler.innerHTML = "&nbsp;";
}