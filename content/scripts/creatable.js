var cdrag = null;
var is_trash_active = false;


function SetCreatableDraggable(x, y, element, is_item, is_new){
	if (is_new === undefined) is_new = false;
	var click_x = x - $(element).offset().left;
	var click_y = y - $(element).offset().top;
	if (is_new){
		element = element.cloneNode(true);
		RemoveStyleFamilyTree(element);
		if (is_item)
			element.id = "item_" + CreatableCountItems();
		else{
			var count = CreatableCountSections();
			var section = element;
			section.id = "section_" + count;
			element = document.createElement('div');
			element.className = "creatable_section_container";
			element.id = "section_" + count + "_container";
			element.appendChild(section);
		}
	}
	
	var draggable = element.cloneNode(true);
	draggable.style.opacity = "0.8";
	draggable.style.filter = "alpha(opacity=80)";
	draggable.style.zIndex = 200;
	element.style.visibility = "hidden";
	draggable.style.position = "absolute";
	draggable.style.left = x - click_x;
	draggable.style.top = y - click_y;
	$("#creatable")[0].appendChild(draggable);
	DisableEditableFamilyTree(draggable);

	cdrag = {
		click_x: click_x
		,click_y: click_y
		,draggable: draggable
		,item: element
		,is_item: is_item
		,is_new: is_new
	};
}

/*******************ACTUAL MOUSE EVENT HANDLERS******************/
function CreatableOnMouseDown(e){
	var x = e.pageX;
	var y = e.pageY;

	var element = null;
	var is_item = false;
	var is_new = false;
	
	//If we're inside a textarea, don't try to move the item
	if (SearchElementsForMouse(x, y, $(".creatable_section_inner")) !== null ||
			SearchElementsForMouse(x, y, $(".creatable_item_text_inner"))){
		return;
	}
	
	//Else, find out if we're trying to drag a section or an item box
	element = SearchElementsForMouse(x, y, $(".creatable_section_outer"));
	if (element === null){
		element = SearchElementsForMouse(x, y, $(".creatable_item_outer"));
		if (element !== null){
			is_item = true;
			
			if (element.id === "new_item"){
				is_new = true;
			}
		}
	}else{
		var classNames = element.className.split(" ");
		if (classNames.indexOf("small_section_header") >= 0){
			if (element.id === "new_small_section"){
				is_new = true;
			}else{
				element = document.getElementById(element.id + "_container");
			}
		}
	}
	
	if (element !== null){
		SetCreatableDraggable(x, y, element, is_item, is_new);
	}
}

function CreatableOnMouseMove(e){
	var x = e.pageX;
	var y = e.pageY;
	
	if (cdrag !== null){		
		cdrag.draggable.style.left = x - cdrag.click_x;
		cdrag.draggable.style.top = y - cdrag.click_y;
		
		TryToDeleteItem(e);
		
		//Arrange items differently than section heads
		if (cdrag.is_item){
			var item = SearchElementsForMouse(x, y, Items(), cdrag.draggable, false);
			//IF USER IS DRAGGING CURRENT ITEM INTO POSITION OF ANOTHER ITEM
			if (item !== null && item.id !== cdrag.item.id){
				//If item isn't on the same row as the item we're trying to swap with
				//We'll append the item to this row first
				if (cdrag.item.parentNode === null || item.parentNode.id !== cdrag.item.parentNode.id){
					cdrag.is_new = false;
					MoveNodeToParent(cdrag.item, item.parentNode);
					ChangeItemColor(cdrag, item.parentNode);
				}
			
				//Swap the two nodes
				ShiftNodes(item, cdrag.item);
			}
			//EVEN IF ITEM ISN"T BEING DRAGGED ONTO ANOTHER ITEM
			//we need to check to see if it is being dragged to the end of a row
			else if (item === null){
				var section = SearchRowsForMouse(x, y);
				if (section !== null && (cdrag.item.parentNode === null || section.id !== cdrag.item.parentNode.id)){
					cdrag.is_new = false;
					MoveNodeToParent(cdrag.item, section);
					ChangeItemColor(cdrag, section);
				}
			}
		}
		//Arrange section heads different than items
		else{
			var section = SearchElementsForMouse(x, y, SectionContainers(), cdrag.draggable);
			if (section !== null && section.id !== cdrag.item.id){
				if (cdrag.is_new)
					MoveNodeToParent(cdrag.item, section.parentNode)
				cdrag.is_new = false;
				//LET'S SWAP THEM...
				ShiftNodes(section, cdrag.item);
			}
		}
	}
}

function CreatableOnMouseUp(e){
	//TODO:: IF NEW ITEM IS A SMALL SECTION, NEED TO MAKE A SECTION CONTAINER HERE???
	if (cdrag !== null){
		$("#creatable")[0].removeChild(cdrag.draggable);
		cdrag.item.removeAttribute("style");
		
		if (is_trash_active){
			cdrag.item.parentNode.removeChild(cdrag.item);
		}
	}
	cdrag = null;
	DeactivateTrashCan();
}

/********************MOUSE EVENT UTILS*********************/
function SearchElementsForMouse(x, y, elems, ignore, search_new){
	if (search_new === undefined) search_new = true;
	for (var i = 0; i < elems.length; i++){
		var e = elems[i];
		if (!search_new && (e.id === "new_item" || e.id === "new_small_section"))
			continue;
		if (e === ignore) continue;
		if ($(e).offset().left < x && $(e).offset().left + $(e).width() > x &&
				$(e).offset().top < y && $(e).offset().top + $(e).height() > y){
			return e;
		}
	}
	return null;
}

function SearchRowsForMouse(x, y, search_new){
	if (search_new === undefined) search_new = true;
	var rows = $(".creatable_section_container");
	for (var i = 0; i < rows.length; i++){
		var r = rows[i];
		if ($(r).offset().left < x && $(r).offset().left + $(r).width() > x &&
				$(r).offset().top < y && $(r).offset().top + $(r).height() > y){
			return r;
		}
	}
	return null;
}

function TryToDeleteItem(e){
	var x = e.pageX;
	var y = e.pageY;
	
	//Look for the trashcan (this needs to be modified a bit
	var trash = SearchElementsForMouse(x, y, $("#trashcan"));
	if (trash !== null){
		ActivateTrashCan();
	}else{ 
		DeactivateTrashCan();
	}
}

function ActivateTrashCan(){
	$("#trashlid")[0].style.marginTop = "-10px";
	$("#trashlid")[0].style.marginBottom = "10px";
	is_trash_active = true;
}

function DeactivateTrashCan(){
	$("#trashlid")[0].removeAttribute("style"); 
	is_trash_active = false;
}