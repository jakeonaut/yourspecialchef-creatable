function CreateNewBigSection(color, text, is_small){
	
}

function CreateNewSection(color, text, is_small){
	var outer_section = document.createElement('div');
	outer_section.className = "creatable_section_outer small_section_header";
	outer_section.className += " " + "outer_color_" + color;
	outer_section.id = "new_small_section";
	
	var middle_section = document.createElement('div');
	middle_section.className = "creatable_section_middle";
	middle_section.className += " " + "middle_color_" + color;
	
	var inner_section = document.createElement('div');
	inner_section.className = "creatable_section_inner";
	inner_section.contentEditable = true;
	inner_section.innerHTML = text;
	
	middle_section.appendChild(inner_section);
	outer_section.appendChild(middle_section);
	if (is_small){
		outer_section.style.width = "96px";
		outer_section.style.height = "96px";
		outer_section.style.fontSize = "20px";
		outer_section.style.lineHeight = "20px";
	}
	return outer_section;
}

function CreateNewItem(color, text, img_src, is_small){
	var outer_section = document.createElement('div');
	outer_section.className = "creatable_item_outer";
	outer_section.className += " " + "outer_color_" + color;
	outer_section.id = "new_item";
	
	var middle_section = document.createElement('div');
	middle_section.className = "creatable_item_middle";
	middle_section.className += " " + "middle_color_" + color;
	
	var image = document.createElement('img');
	image.className = "creatable_item_pict_inner";
	image.src = img_src;
	image.draggable = false;
	
	var inner_text = document.createElement('div');
	inner_text.className = "creatable_item_text_inner";
	inner_text.contentEditable = true;
	inner_text.innerHTML = text;
	
	if (is_small){	
		image.style.width = "70px";
		image.style.height = "70px";
		
		inner_text.style.minHeight = "23px";
		
		outer_section.style.width = "80px";
		outer_section.style.height = "96px";
		outer_section.style.fontSize = "16px";
		outer_section.style.lineHeight = "16px";
	}
	
	middle_section.appendChild(image);
	middle_section.appendChild(inner_text);
	outer_section.appendChild(middle_section);
	return outer_section;
}