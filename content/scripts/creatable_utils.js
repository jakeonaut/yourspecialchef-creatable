function Sections(){ return $(".creatable_section_outer"); }
function SectionContainers(){ return $(".creatable_section_container"); }
function Items(){ return $(".creatable_item_outer"); }

//http://stackoverflow.com/questions/4406206/how-to-swap-position-of-html-tags
function SwapNodes(node1, node2){
	node2_copy = node2.cloneNode(true);
	node1.parentNode.insertBefore(node2_copy, node1);
	node2.parentNode.insertBefore(node1, node2);
	node2.parentNode.replaceChild(node2, node2_copy);
}

function ShiftNodes(node1, node2){
	var parentNode = node1.parentNode;
	var node1index = 0;
	var node2index = 0;
	for (var i = 0; i < parentNode.children.length; i++){
		if (parentNode.children[i] === node1)
			node1index = i;
		if (parentNode.children[i] === node2)
			node2index = i;
	}
	
	if (node1index < node2index){
		for (var i = node2index; i > node1index; i--){
			SwapNodes(parentNode.children[i], parentNode.children[i-1]);
		}
	}else{
		for (var i = node2index; i < node1index; i++){
			SwapNodes(parentNode.children[i], parentNode.children[i+1]);
		}
	}
}

function MoveNodeToParent(node, parent){
	if (node.parentNode !== null)
		node.parentNode.removeChild(node);
	if (parent !== null)
		parent.appendChild(node);
}

function ChangeItemColor(cdrag, section){
	var section_header = section.children[0];
	var outer_color = "";
	var middle_color = "";
	
	var classes = section_header.className.split(' ');
	for (var i = 0; i < classes.length; i++){
		if (classes[i].indexOf("outer_color") >= 0)
			outer_color = classes[i];
		if (classes[i].indexOf("middle_color") >= 0)
			middle_color = classes[i];
	}
	
	classes = cdrag.item.className.split(' ');
	for (var i = 0; i < classes.length; i++){
		if (classes[i].indexOf("outer_color") >= 0)
			classes[i] = outer_color;
		if (classes[i].indexOf("middle_color") >= 0)
			classes[i] = middle_color;
	}
	cdrag.item.className = classes.join(' ');
	cdrag.draggable.className = classes.join(' ');
}

function RemoveStyleFamilyTree(parent){
	if (parent.removeAttribute !== undefined)
		parent.removeAttribute("style");
	var children = parent.childNodes;
	for (var i = 0; i < children.length; i++){
		RemoveStyleFamilyTree(children[i]);
	}
}

function DisableEditableFamilyTree(parent){
	parent.contentEditable = false;
	var children = parent.childNodes;
	for (var i = 0; i < children.length; i++){
		DisableEditableFamilyTree(children[i]);
	}
}

function CreatableCountItems(is_item){
	//Find the next available id (skipping ones that have been deleted *shrug*
	var count = 0;
	var items = [];
	if (is_item || is_item == undefined)
		items = $(".creatable_item_outer");
	else items = $(".creatable_section_outer");
	for (var i = 0; i < items.length; i++){
		var id = items[i].id;
		if (is_item)
			id = id.substr("item_".length);
		else id = id.substr("section_".length);
		id = parseInt(id);
		if (id >= count){
			count = id+1;
		}
	}
	return count;
}

function CreatableCountSections(){ return CreatableCountItems(false); }